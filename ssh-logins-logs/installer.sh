#!/bin/bash


echo "sudo ls to get super user rights"
sudo ls

WORKSPACE_LOCATION="/etc/linux-workspace/ssh-login-notification"
FILENAME="notify.sh"

sudo mkdir -p $WORKSPACE_LOCATION

echo "Find your slack hook url (https://hooks.slack.com/services/XXXXX/YYYYY/ZZZZ)"
echo "If you do not have a slack app yet, go to https://api.slack.com/apps?new_app=1"
read -r -p "[url] ? " SLACK_HOOK_URL

curl -s -L https://bitbucket.org/SolidSnake--23/linux-workspace/raw/master/ssh-logins-logs/notify.sh |  cat > /tmp/"$FILENAME"

sed "s,SLACK_HOOK_URL_PLACEHOLDER,$SLACK_HOOK_URL,g" /tmp/"$FILENAME" | sudo tee "$WORKSPACE_LOCATION/$FILENAME"

sudo chmod +x "$WORKSPACE_LOCATION/$FILENAME"

rm /tmp/"$FILENAME"

printf '\nForceCommand "%s"\n' "$WORKSPACE_LOCATION/$FILENAME" | sudo tee -a /etc/ssh/sshd_config

while true; do
    read -r -p "Do you want to restart ssh daemon? " yn
    case $yn in
        [Yy]* ) sudo service sshd restart;;
        [Nn]* ) exit;;
        * ) echo "Please answer y or n.";;
    esac
done
