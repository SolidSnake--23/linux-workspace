#!/bin/bash

url="SLACK_HOOK_URL_PLACEHOLDER"

host="$(hostname)"

content="\"attachments\": [ { \"mrkdwn_in\": [\"text\", \"fallback\"], \"fallback\": \"SSH login: $USER connected to \`$host\`\", \"text\": \"SSH login to \`$host\`\", \"fields\": [ { \"title\": \"User\", \"value\": \"$USER\", \"short\": true }, { \"title\": \"IP Address\", \"value\": \"$SSH_CLIENT\", \"short\": true } ], \"color\": \"#6B8E23\" } ]"

curl -s -X POST -H "Content-Type: application/json" --data "{\"mrkdwn\": true, $content, \"icon_emoji\": \":computer:\"}" $url